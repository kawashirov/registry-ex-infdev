package net.in.voa.rex.util;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public final class IPSet implements Set<InetAddress> {
	
	private static class Node {
		public static final int PTR_ROOT = 0;
		public static final int PTR_NULL = 0;
		public static final int PTR_FINAL = -1;
		public static final int CHILD_NULL = -1;
		public int parent, count = 0;
		public int[] childs = new int[256];
		
		public Node( int parent ) {
			this.parent = parent;
		}
		
	}
	
	private class BorIterator implements Iterator<InetAddress> {
		private boolean interrupted = false;
		private boolean finished = false;
		private int[] stack_ptr = new int[length_bytes];
		private int[] stack_child = new int[length_bytes];
		
		public BorIterator( ) {
			int ptr_down = Node.PTR_ROOT;
			if ( bor.get(ptr_down).count == 0 ) // Пусто, делать нечего
				return;
			// Что-то есть, надо найти!
			for ( int deepth = 0; deepth < length_bytes; ++deepth ) {
				final Node ptr_node = bor.get(ptr_down);
				stack_ptr[deepth] = ptr_down;
				int child = 0;
				while ( child < 256 && ptr_node.childs[child] != Node.PTR_NULL )
					++child;
				assert ptr_node.childs[child] != Node.PTR_NULL; // Должен найтись!
				stack_child[deepth] = child;
			}
			active_iterators.add(this);
		}
		
		@Override
		public boolean hasNext() {
			return !interrupted && !finished;
		}
		
		@Override
		public InetAddress next() {
			if ( interrupted ) throw new ConcurrentModificationException();
			
			// Сборка адреса
			InetAddress adr = null;
			byte[] bytes = new byte[length_bytes];
			for ( int deepth = 0; deepth < length_bytes; ++deepth )
				bytes[deepth] = (byte) (stack_child[deepth] + 128);
			try {
				adr = InetAddress.getByAddress(bytes);
			} catch ( Exception e ) {
				assert false;
			}
			
			// Поиск следующего
			int deepth = length_bytes, ptr = Node.PTR_NULL, child = Node.CHILD_NULL;
			// Если что - всплываем, пока можем
			while ( deepth >= 0 ) {
				ptr = stack_ptr[deepth];
				child = stack_child[deepth];
				// В каждом ряду ищем следующий
				while ( child < 256 && bor.get(ptr).childs[child] != Node.PTR_NULL )
					++child;
				// Если нашли - заканчиваем всплывать.
				if ( child != Node.CHILD_NULL ) break;
				// Если нет - всплываем
				--deepth;
			}
			
			if ( child == Node.CHILD_NULL ) {
				// Если ничего не нашли, то больше элементов нет.
				assert deepth == 0;
				finished = true;
			} else {
				// Если нашли
				assert ptr != Node.PTR_NULL;
				stack_child[deepth] = child;
				stack_ptr[deepth] = ptr;
				// Надо снова утонуть.
				int ptr_down = bor.get(ptr).childs[child]; // Первый дочерний
				++deepth;
				while ( deepth < length_bytes ) {
					// Тонем, пока можем
					stack_ptr[deepth] = ptr_down;
					Node node = bor.get(ptr_down);
					// Ищем первый дочерний
					child = 0;
					while ( child < 256 && node.childs[child] == Node.PTR_NULL )
						++child;
					assert node.childs[child] != Node.PTR_NULL;
					stack_child[deepth] = child;
					ptr_down = node.childs[child];
					++deepth;
				}
				// Глубина должна совпасть!
				// assert deepth_down == length_bytes;
			}
			
			return adr;
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	private final int length_bytes;
	private final List<Node> bor;
	private final LinkedList<BorIterator> active_iterators = new LinkedList<>();
	
	public IPSet( int lengthBytes, int initialCapacity ) {
		length_bytes = lengthBytes;
		bor = new ArrayList<>(initialCapacity);
		final Node root = new Node(Node.PTR_FINAL);
		bor.add(0, root);
	}
	
	@Override
	public int size() {
		return bor.get(0).count;
	}
	
	@Override
	public boolean isEmpty() {
		return bor.get(0).count == 0;
	}
	
	@Override
	public boolean contains(Object o) {
		if ( !(o instanceof InetAddress) ) return false;
		byte[] bytes = ((InetAddress) o).getAddress();
		if ( bytes.length != length_bytes ) return false;
		int ptr = Node.PTR_ROOT;
		for ( int i = 0; i < length_bytes; ++i ) {
			final Node ptr_node = bor.get(ptr);
			final int child = bytes[i] + 128;
			if ( ptr_node.childs[child] == Node.PTR_NULL ) return false;
			ptr = ptr_node.childs[child];
		}
		assert ptr == Node.PTR_FINAL;
		return true;
	}
	
	@Override
	public Iterator<InetAddress> iterator() {
		return new BorIterator();
	}
	
	@Override
	public InetAddress[] toArray() {
		int count = bor.get(0).count;
		InetAddress[] array = new InetAddress[count];
		int i = 0;
		for ( InetAddress adr : this )
			array[i++] = adr;
		assert i == count;
		return array;
	}
	
	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean add(InetAddress e) {
		final byte[] bytes = e.getAddress();
		if ( bytes.length != length_bytes ) throw new IllegalArgumentException();
		
		for ( BorIterator itr : active_iterators )
			itr.interrupted = true;
		active_iterators.clear();
		
		int ptr = Node.PTR_ROOT;
		for ( int i = 0; i < length_bytes; ++i ) {
			final Node ptr_node = bor.get(ptr);
			final int child = bytes[i] + 128;
			if ( i < length_bytes - 1 ) {
				// Сейчас НЕ последняя втека
				if ( ptr_node.childs[child] == Node.PTR_NULL ) {
					// Нужна новая дочерняя
					Node new_node = new Node(ptr);
					int new_id = bor.size();
					bor.add(new_id, new_node);
					ptr_node.childs[child] = new_id;
				}
				ptr = ptr_node.childs[child]; // Заходим вглубь
			} else {
				// Сейчас последний узел -> Новые дочерние не нужны.
				if ( ptr_node.childs[child] == Node.PTR_FINAL ) {
					// Уже добавляли
					return false;
				} else {
					// Увеличиваем счётчики
					int ptr_back = ptr;
					while ( ptr_back != Node.PTR_FINAL ) {
						Node n = bor.get(ptr_back);
						++n.count;
						ptr_back = n.parent;
					}
					ptr_node.childs[child] = Node.PTR_FINAL;
					// Готово.
					return true;
				}
			}
		}
		assert false;
		return false;
	}
	
	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean addAll(Collection<? extends InetAddress> c) {
		boolean flag = false;
		for ( InetAddress adr : c )
			if ( add(adr) ) flag = true;
		return flag;
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void clear() {
		bor.clear();
		final Node root = new Node(Node.PTR_FINAL);
		bor.add(0, root);
	}
	
}
