package net.in.voa.rex;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.common.collect.Iterables;

// TODO тут вообще всю архитектуру передать
public class VoaResolver {
	private static final Logger LOG = Logger.getLogger(VoaResolver.class.getName());
	
	private static class VoaRecord {
		public static byte IPV4 = 4;
		public static byte IPV6 = 6;
		public static byte DOMAIN = 1;
		public byte type = 0;
		public String value = null;
		
		public VoaRecord( byte type_, String value_ ) {
			type = type_;
			value = value_;
		}
	}
	
	// TODO сделать адекватно
	private static class ServerGroupResolver extends Thread {
		private static final Logger LOG = Logger.getLogger(ServerGroupResolver.class.getName());
		private final List<String> servers;
		private final String[] cmd_base;
		private final List<VoaRecord> answer;
		private final String what;
		
		public ServerGroupResolver( List<String> servers, String what, List<VoaRecord> answers ) {
			this.servers = servers;
			this.what = what.toLowerCase();
			this.cmd_base = new String[] { "host", "-t", "ANY", "-W", "1", "-R", "5", "-T", this.what, null };
			this.answer = answers;
		}
		
		@Override
		public void run() {
			for ( String server : servers )
				try {
					cmd_base[cmd_base.length - 1] = server;
					Process exec = Runtime.getRuntime().exec(cmd_base);
					int code = exec.waitFor();
					if ( code != 0 ) {
						LOG.warning("One of \"" + what.toString() + "\" processes returned non-zero exit code: " + code);
						// На следующий сервер в случае провала.
						continue;
					}
					InputStreamReader isreader = new InputStreamReader(exec.getInputStream());
					BufferedReader reader = new BufferedReader(isreader);
					while ( true ) {
						String line = reader.readLine();
						LOG.finest("Answer: \"" + line + "\".");
						if ( line == null ) break;
						try {
							if ( line.contains(" has address ") ) {
								String part = line.split(" has address ")[1];
								LOG.fine("IPv4 discovered: \"" + part + "\".");
								answer.add(new VoaRecord(VoaRecord.IPV4, part));
							} else if ( line.contains(" has IPv6 address ") ) {
								String part = line.split(" has IPv6 address ")[1];
								LOG.fine("IPv6 discovered: \"" + part + "\".");
								answer.add(new VoaRecord(VoaRecord.IPV6, part));
							} else if ( Config.DNS_DISCOVER_NEW_DOMAINS ) {
								String name_s = null;
								if ( line.contains(" is an alias for ") ) {
									name_s = line.split(" is an alias for ")[1];
								} else if ( line.contains(" domain name pointer ") ) {
									name_s = line.split(" domain name pointer ")[1];
								} else if ( line.contains(" mail is handled by ") ) {
									name_s = line.split(" mail is handled by [0-9]* ")[1];
								}
								if ( name_s != null ) {
									name_s = name_s.toLowerCase();
									if ( name_s.charAt(name_s.length() - 1) != '.' ) name_s = name_s + '.';
									if ( !name_s.endsWith(this.what) ) {
										LOG.fine("Domain discovered: \"" + name_s + "\".");
										answer.add(new VoaRecord(VoaRecord.DOMAIN, name_s));
									}
								}
							}
						} catch ( Exception parseException ) {
							LOG.log(Level.WARNING, "Could not parse line \"" + line + "\": ", parseException);
							parseException.printStackTrace();
						}
					}
					reader.close();
					isreader.close();
					exec.destroy();
					// В случае успеха, дургие сервера не нужны.
					break;
				} catch ( Exception processException ) {
					LOG.log(Level.SEVERE, "Could execute coomand for \"" + what + "\": ", processException);
					processException.printStackTrace();
					// Не обязательно освобождать ресурсы: подразумеваем, что программа крэшится :с
					throw new Error("", processException);
				}
		}
		
	}
	
	public VoaResolver( ) {}
	
	private List<VoaRecord> get_records(String what) {
		LOG.fine("Resolving \"" + what + "\"...");
		List<VoaRecord> answers = new LinkedList<VoaRecord>();
		List<ServerGroupResolver> resolvers = new ArrayList<>(Config.DNS_SERVERS.size());
		// Здесь ветвение на потоки: какждая группа серверов резолвится в своём потоке, все результаты скидываются в
		// answers, возможно, дублируясь. В группе достаточно одного успешного запроса.
		LOG.finest("Building threads for \"" + what + "\"...");
		for ( List<String> servers : Config.DNS_SERVERS )
			resolvers.add(new ServerGroupResolver(servers, what, Collections.synchronizedList(answers)));
		LOG.finest("Stating threads for \"" + what + "\"...");
		for ( ServerGroupResolver sgr : resolvers )
			sgr.start();
		LOG.finest("Joining threads for \"" + what + "\"...");
		for ( ServerGroupResolver sgr : resolvers )
			try {
				sgr.join();
			} catch ( InterruptedException e ) {}
		return answers;
	}
	
	public void domains2ips(Registry registry) {
		LinkedList<String> queue = new LinkedList<>();
		Set<String> discovered = new HashSet<>();
		int ipv4 = 0, ipv6 = 0;
		LOG.info("Resolving " + registry.domains.size() + " domains to IPs...");
		int ii = 0;
		for ( String basename : registry.domains ) {
			queue.clear();
			queue.offer(basename);
			int i = 0;
			while ( i++ < Config.DNS_MAX_DISCOVERED_DOMAINS && !queue.isEmpty() ) {
				String name = queue.poll();
				for ( VoaRecord record : get_records(name.toString()) )
					try {
						if ( record.type == VoaRecord.IPV4 ) {
							if ( registry.ipv4.add(IP.new4(record.value)) ) ++ipv4;
						} else if ( record.type == VoaRecord.IPV6 ) {
							if ( registry.ipv6.add(IP.new6(record.value)) ) ++ipv6;
						} else if ( record.type == VoaRecord.DOMAIN ) {
							if ( discovered.add(record.value) ) queue.offer(record.value);
						}
					} catch ( Exception e ) {}
			}
			if ( i == Config.DNS_MAX_DISCOVERED_DOMAINS && LOG.isLoggable(Level.WARNING) )
				LOG.warning("Discovering limit (" + Config.DNS_MAX_DISCOVERED_DOMAINS + ") reached for doamin \""
						+ basename.toString() + "\".");
			++ii;
			LOG.info("Progress: " + (100 * ii / registry.domains.size()) + "%...");
		}
		LOG.info("Discovered new: IPv4=" + ipv4 + ", IPv6=" + ipv6 + ", domains=" + discovered.size() + ".");
		registry.domains.addAll(discovered);
		
	}
	
	private String ip2domain(IP addr) {
		return null;
	}
	
	public void ips2domains(Registry registry) {
		for ( IP ip : Iterables.concat(registry.ipv4, registry.ipv6) ) {
			String discovered = ip2domain(ip);
			if ( discovered != null ) registry.domains.add(discovered);
		}
	}
	
}
