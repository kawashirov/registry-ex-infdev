package net.in.voa.rex;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import com.google.common.collect.Lists;

public class Config {
	// Main
	public static final String LOCK_FILE = "registry.lock";
	public static final Level LOG_LEVEL = Level.ALL;
	// Sources
	public static final String SRC_CUSTOM_FILE = "custom.txt";
	// DNS
	public static final boolean DNS_DO_DOMAIN_TO_IP_RESOLVING = true;
	public static final boolean DNS_DO_IP_TO_DOMAIN_RESOLVING = true;
	public static final boolean DNS_DISCOVER_NEW_DOMAINS = true;
	/** Сколько новых доменов может порадить один домен. */
	public static final int DNS_MAX_DISCOVERED_DOMAINS = 10;
	// Не используется
	public static final int DNS_RETRIES = 5;
	/** В миллисекундах */
	// Не используется
	public static final int DNS_TIMEOUT = 500;
	@SuppressWarnings( "unchecked" )
	public static final List<List<String>> DNS_SERVERS = Collections.unmodifiableList(Lists.newArrayList( //
			Collections.unmodifiableList(Lists.newArrayList( // Yandex
					"77.88.8.1", "77.88.8.8", "2a02:6b8:0:1::feed:0ff", "2a02:6b8::feed:0ff")), //
			Collections.unmodifiableList(Lists.newArrayList(// Google
					"8.8.4.4", "8.8.8.8", "2001:4860:4860::8844", "2001:4860:4860::8888")), //
			Collections.unmodifiableList(Lists.newArrayList(// OpenDNS
					"208.67.220.220", "208.67.220.222", "2620:0:ccd::2", "2620:0:ccc::2")), //
			Collections.unmodifiableList(Lists.newArrayList(// Hurricane Electric
					"74.82.42.42", "2001:470:20::2")) //
			));
	// Не используется
	public static final String DNS_CAHCHE_FILE = "dnsrecords.cache";
	// Не используется
	public static final int DNS_CACHE_MAX_TTL = 60 * 60 * 24 * 3;
	// Не используется
	public static final int DNS_CACHE_MAX_NEGATE_TTL = 60 * 60;
	
	//
	private Config( ) {}
}
