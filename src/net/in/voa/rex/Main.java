package net.in.voa.rex;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Logger;

public final class Main {
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		// Сферический велосипед в вакууме.
		for ( Handler h : Logger.getLogger("").getHandlers() )
			h.setLevel(Config.LOG_LEVEL);
		
		final List<Source> sources = new LinkedList<Source>();
		final Registry registry = new Registry();
		final VoaResolver resolver = new VoaResolver();
		
		sources.addAll(Custom_Source.get());
		
		for ( Source source : sources )
			source.load(registry);
		
		if ( Config.DNS_DO_DOMAIN_TO_IP_RESOLVING ) resolver.domains2ips(registry);
		// if ( Config.DNS_DO_IP_TO_DOMAIN_RESOLVING ) resolver.ips2domains(registry);
		
		for ( String a : registry.domains )
			System.out.println(a.toString());
		System.out.println();
		for ( IP.V4 a : registry.ipv4 )
			System.out.println(a.toString());
		System.out.println();
		for ( IP.V6 a : registry.ipv6 )
			System.out.println(a.toString());
		
		System.out.println("Stop!");
	}
	
}
