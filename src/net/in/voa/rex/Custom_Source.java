package net.in.voa.rex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Custom_Source extends Source {
	private static final Logger LOG = Logger.getLogger(Custom_Source.class.getName());
	private static Custom_Source instance = null;
	
	public static Set<Custom_Source> get() {
		if ( instance == null ) instance = new Custom_Source();
		return Collections.singleton(instance);
	}
	
	@Override
	public void load(Registry registry) {
		if ( registry != null ) try ( BufferedReader br = new BufferedReader(new FileReader(Config.SRC_CUSTOM_FILE)) ) {
			LOG.log(Level.INFO, "Loafing records from file \"" + Config.SRC_CUSTOM_FILE + "\"... ");
			String line;
			int i = 0;
			do {
				line = br.readLine();
				if ( !isComment(line) ) {
					++i;
					LOG.log(Level.FINE, "Read record: " + line);
					registry.add(line);
				}
			} while ( line != null );
			LOG.log(Level.INFO, "Loaded " + i + " records.");
		} catch ( Exception e ) {
			LOG.log(Level.SEVERE, "Can't open file \"" + Config.SRC_CUSTOM_FILE + "\": ", e);
			e.printStackTrace();
		}
	}
	
}
