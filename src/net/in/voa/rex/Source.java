package net.in.voa.rex;


public abstract class Source {
	
	protected boolean isComment(final String line) {
		return line == null || line.isEmpty() || line.charAt(0) == '#' || line.charAt(0) == ';';
	}
	
	abstract public void load(Registry registry);
	
}
