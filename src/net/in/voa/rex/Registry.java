package net.in.voa.rex;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import net.in.voa.rex.util.InetAddressUtils;

public class Registry {
	private static final Pattern DOMAIN_DOT = Pattern.compile("\\.");
	private static final Pattern DOMAIN_PART = Pattern.compile("^[0-9a-zA-Z-]{1,63}$");
	private static final Comparator<String> COMAPRATOR = new Comparator<String>() {
		@Override
		public int compare(String a, String b) {
			final int al = a.length(), bl = b.length();
			final int min = Math.min(al, bl);
			for ( int i = 1; i <= min; ++i ) {
				if ( a.charAt(al - i) < b.charAt(bl - i) ) return -1;
				else if ( a.charAt(al - i) > b.charAt(bl - i) ) return 1;
			}
			return al < bl ? -1 : (al > bl ? 1 : 0);
		}
	};
	public final SortedSet<IP.V4> ipv4 = new TreeSet<IP.V4>();
	public final SortedSet<IP.V6> ipv6 = new TreeSet<IP.V6>();
	public final SortedSet<String> domains = new TreeSet<String>(COMAPRATOR);
	
	public void add(String data) {
		data = data.trim();
		if ( InetAddressUtils.isIPv4Address(data) ) {
			// IPv4
			ipv4.add(IP.new4(data));
		} else if ( InetAddressUtils.isIPv6Address(data) ) {
			// IPv6
			ipv6.add(IP.new6(data));
		} else {
			// Домены
			// Проверка
			String[] parts = DOMAIN_DOT.split(data);
			for ( int i = 0; i < parts.length; ++i ) {
				if ( i == parts.length - 1 && parts[i].length() == 0 ) continue;
				if ( parts[i].charAt(0) == '-' || parts[i].charAt(parts[i].length() - 1) == '-'
						|| !DOMAIN_PART.matcher(parts[i]).matches() ) return;
			}
			// Дописываем точку в конец.
			if ( data.charAt(data.length() - 1) != '.' ) data = data + '.';
			// Откусываем www. и mx. в начале.
			if ( data.startsWith("www.") ) data = data.substring(4, data.length());
			//if ( data.startsWith("mx.") ) data = data.substring(3, data.length());
			domains.add(data);
		}
	}
	
}
