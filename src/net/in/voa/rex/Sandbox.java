package net.in.voa.rex;

public class Sandbox {
	
	public static void main(String[] args) throws Exception {
		
		for ( String s : "youtube.com mail is handled by 20 alt1.aspmx.l.google.com".split(" mail is handled by [0-9]* ") )
			System.out.println(s);
		
		for ( byte i = -128; i <= 126; ++i )
			System.out.println(i & 0xFF);
		
	}
}
