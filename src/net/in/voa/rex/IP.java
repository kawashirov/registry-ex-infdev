package net.in.voa.rex;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;

// TODO подсети!!!
public abstract class IP {
	// Потому что мне не нравятся InetAddress
	
	public static class V4 extends IP implements Comparable<V4> {
		public static final byte FAMILY = 4;
		public static final byte BYTES = 4;
		
		private V4( byte[] data ) {
			super(data, FAMILY, BYTES, BYTES * 8);
		}
		
		private V4( byte[] data, int net ) {
			super(data, FAMILY, BYTES, net);
		}
		
		@Override
		public int compareTo(V4 other) {
			return super.compareTo(other);
		}
		
		@Override
		public String toString() {
			return (data[0] & 0xff) + "." + (data[1] & 0xff) + "." + (data[2] & 0xff) + "." + (data[3] & 0xff);
		}
		
	}
	
	public static class V6 extends IP implements Comparable<V6> {
		public static final byte FAMILY = 6;
		public static final byte BYTES = 16;
		
		private V6( byte[] data ) {
			super(data, FAMILY, BYTES, BYTES * 8);
		}
		
		private V6( byte[] data, int net ) {
			super(data, FAMILY, BYTES, net);
		}
		
		@Override
		public int compareTo(V6 other) {
			return super.compareTo(other);
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(40);
			for ( int i = 0; i < BYTES; i += 2 ) {
				sb.append(Integer.toHexString(((data[i] & 0xff) << 8) | (data[i + 1] & 0xff)));
				if ( i < BYTES - 2 ) sb.append(':');
			}
			return sb.toString();
		}
	}
	
	protected final byte family;
	protected final byte bytes;
	protected int net;
	protected byte[] data;
	
	public static V4 new4(String host) {
		try {
			for ( InetAddress stdip : InetAddress.getAllByName(host) )
				if ( stdip instanceof Inet4Address ) return new V4(stdip.getAddress());
		} catch ( Exception e ) {}
		return null;
	}
	
	public static V6 new6(String host) {
		try {
			for ( InetAddress stdip : InetAddress.getAllByName(host) )
				if ( stdip instanceof Inet6Address ) return new V6(stdip.getAddress());
		} catch ( Exception e ) {}
		return null;
	}
	
	public static IP newAny(String host) {
		try {
			for ( InetAddress stdip : InetAddress.getAllByName(host) ) {
				if ( stdip instanceof Inet4Address ) return new V4(stdip.getAddress());
				if ( stdip instanceof Inet6Address ) return new V6(stdip.getAddress());
			}
		} catch ( Exception e ) {}
		return null;
	}
	
	protected IP( byte[] data, byte family, byte length, int net ) {
		if ( data.length != length ) throw new Error();
		this.data = data;
		this.family = family;
		this.bytes = length;
		this.net = net;
	}
	
	private int compareTo(IP other) {
		for ( int i = 0; i < data.length; ++i )
			if ( (data[i] & 0xFF) > (other.data[i] & 0xFF) ) return 1;
			else if ( (data[i] & 0xFF) < (other.data[i] & 0xFF) ) return -1;
		return 0;
	}
	
}
